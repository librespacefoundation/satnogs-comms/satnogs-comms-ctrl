/*
 *  SatNOGS-COMMS CLI control software
 *
 *  Copyright (C) 2023-2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#include "transport.hpp"

transport::transport(logger::sptr logger) : m_logger(logger) {}

std::string
transport::get_time_iso8601()
{
  std::chrono::system_clock::time_point tp = std::chrono::system_clock::now();
  return date::format("%FT%TZ", date::floor<std::chrono::microseconds>(tp));
}

uint64_t
transport::get_timestamp_ms()
{
  auto now = std::chrono::system_clock::now();
  date::sys_time<std::chrono::milliseconds> tp =
      date::floor<std::chrono::milliseconds>(now);
  return tp.time_since_epoch().count();
}
