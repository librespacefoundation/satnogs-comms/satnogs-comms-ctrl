/*
 *  SatNOGS-COMMS CLI control software
 *
 *  Copyright (C) 2023-2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#pragma once

#include "date.h"
#include "logger.hpp"
#include <chrono>
#include <memory>
#include <satnogs-comms-proto/satnogs-comms.pb.h>

class transport
{
public:
  using sptr = std::shared_ptr<transport>;

  transport(logger::sptr logger);

  virtual void
  send(const satnogs_comms &msg) = 0;

  virtual void
  recv(satnogs_comms &msg) = 0;

  virtual void
  shutdown() = 0;

  static std::string
  get_time_iso8601();

  static uint64_t
  get_timestamp_ms();

  virtual size_t
  max_msg_len() const = 0;

protected:
  logger::sptr m_logger;
};