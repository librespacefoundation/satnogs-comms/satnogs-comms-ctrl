/*
 *  SatNOGS-COMMS CLI control software
 *
 *  Copyright (C) 2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#include "logger.hpp"
#include "transport.hpp"
#include <google/protobuf/util/json_util.h>
#include <nlohmann/json.hpp>
#include <spdlog/sinks/basic_file_sink.h>
#include <spdlog/sinks/stdout_color_sinks.h>

logger::sptr
logger::make_shared(const std::string &send_log_fname,
                    const std::string &recv_log_fname)
{
  return logger::sptr(new logger(send_log_fname, recv_log_fname));
}

void
logger::send(std::initializer_list<target> list, const satnogs_comms &msg)
{
  std::string    send_msg;
  nlohmann::json j;
  std::string    s;
  for (auto i : list) {
    switch (i) {
    case target::CONSOLE:
      console(msg);
      break;
    case target::FILE:
      google::protobuf::util::MessageToJsonString(msg, &s);
      j                      = nlohmann::json::parse(s);
      j["timestamp-iso8601"] = transport::get_time_iso8601();
      j["timestamp"]         = transport::get_timestamp_ms();
      send_msg               = j.dump();
      m_send_log->info(send_msg);
      break;
    default:
      break;
    }
  }
}

/**
 * @brief Log a sent message to all available loggers
 *
 * @param msg the message to log
 */
void
logger::send(const satnogs_comms &msg)
{
  send({target::CONSOLE, target::FILE}, msg);
}

void
logger::recv(std::initializer_list<target> list, const satnogs_comms &msg)
{
  nlohmann::json j;
  std::string    recv_msg;
  std::string    s;
  for (auto i : list) {
    switch (i) {
    case target::CONSOLE:
      console(msg);
      break;
    case target::FILE:
      google::protobuf::util::MessageToJsonString(msg, &s);
      j                      = nlohmann::json::parse(s);
      j["timestamp-iso8601"] = transport::get_time_iso8601();
      j["timestamp"]         = transport::get_timestamp_ms();
      recv_msg               = j.dump();
      m_send_log->info(recv_msg);
      break;
    default:
      break;
    }
  }
}

/**
 * @brief Logs a received message to all available loggers
 *
 * @param msg the received message to log
 */
void
logger::recv(const satnogs_comms &msg)
{
  recv({target::CONSOLE, target::FILE}, msg);
}

/**
 * @brief Logs to console using a more human-friendly format
 *
 * @param msg the message to log
 */
void
logger::console(const satnogs_comms &msg)
{
  google::protobuf::util::JsonPrintOptions options;
  options.always_print_primitive_fields = true;
  options.add_whitespace                = true;
  options.preserve_proto_field_names    = true;

  std::string s;
  google::protobuf::util::MessageToJsonString(msg, &s, options);
  m_console_log->info(s);
}

logger::logger(const std::string &send_log_fname,
               const std::string &recv_log_fname)
    : m_send_log(spdlog::basic_logger_mt("tx-file-log", send_log_fname)),
      m_recv_log(spdlog::basic_logger_mt("rx-file-log", recv_log_fname)),
      m_console_log(spdlog::stdout_color_mt("console-log"))
{
  m_send_log->set_pattern("%v");
  m_recv_log->set_pattern("%v");
  m_console_log->set_pattern("%^[%Y-%m-%dT%H:%M:%S.%e%z] [%l] %v%$");
}