# satnogs-comms-ctrl: A command line utility to control the SatNOGS-COMMS

This tool allows the control of the SatNOGS-COMMS board via a command-line interface.

> **This tool is deprecated!**: Please use the [YAMCS-based control](https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-yamcs) instead

## Requirements
* CMake (>= 3.20)
* C++17
* GNU Make
* Linux CAN support
* libboost-system
* protoc
* spdlog
* yaml-cpp (>= 0.8.0)
* GNU Radio (>= 3.10.0)
* cppzmq


## Build instructions

1. Clone the repository with all the necessary submodules
  ```bash
  git clone --recurse-submodules https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-ctrl.git
  ```
2. Configure and build the program:

  ```bash
  cd satnogs-comms-ctrl
  mkdir build
  cd build
  cmake ..
  make
  ```

## Usage
The tool uses a `YAML` configuration file for the majority of the parameters.
Note that some command line parameters have priority over the corresponding in the configuration file.


```bash
Usage: satnogs-comms-ctrl [options] ARG1 ARG2
    -h, --help
        shows this help message
    -c, --config
        File path of the configuration file. Explicit parameters have priority over the parameters set in the configuration file
    --log-recv
        filename to store the log of the received messages
    --log-send
        filename to store the log of the sent messages
```
The tool provides a CLI with auto-completion support, multiple levels of operation and help menus for the available commands.

![cli-usage](docs/assets/satnogs-comms-ctrl.gif)

## Website and Contact
For more information about the project and Libre Space Foundation please visit our [site](https://libre.space/)
and our [community forums](https://community.libre.space).
You can also chat with the SatNOGS-COMMS development team at
https://riot.im/app/#/room/#satnogs-comms:matrix.org

## License
![Libre Space Foundation](docs/assets/LSF_HD_Horizontal_Color1-300x66.png)
&copy; 2019-2020 [Libre Space Foundation](https://libre.space).
