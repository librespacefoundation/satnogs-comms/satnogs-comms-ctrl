cmake_minimum_required(VERSION 3.20)
project(satnogs-comms-ctrl CXX C)

include(FetchContent)

FetchContent_Declare(
  json
  URL https://github.com/nlohmann/json/releases/download/v3.11.3/json.tar.xz)
FetchContent_MakeAvailable(json)

set(CMAKE_CXX_STANDARD 17)

set(CMAKE_EXPORT_COMPILE_COMMANDS TRUE)

# ##############################################################################
# CMake custom modules
# ##############################################################################
list(INSERT CMAKE_MODULE_PATH 0 ${CMAKE_SOURCE_DIR}/cmake)

# uninstall target
configure_file(
  "${CMAKE_CURRENT_SOURCE_DIR}/cmake/cmake_uninstall.cmake.in"
  "${CMAKE_CURRENT_BINARY_DIR}/cmake/cmake_uninstall.cmake" IMMEDIATE @ONLY)

add_custom_target(
  uninstall COMMAND ${CMAKE_COMMAND} -P
                    ${CMAKE_CURRENT_BINARY_DIR}/cmake/cmake_uninstall.cmake)

# Mandatory requirements
find_package(Threads REQUIRED)
find_package(Boost 1.69.0 REQUIRED COMPONENTS system)
find_package(yaml-cpp REQUIRED)
find_package(spdlog REQUIRED)
find_package(cppzmq REQUIRED)
find_package(Gnuradio "3.10" REQUIRED COMPONENTS pmt)

# Protobuf and CMake are in a weird state currently...
set(CMAKE_FIND_PACKAGE_PREFER_CONFIG ON)
find_package(Protobuf REQUIRED)

# ##############################################################################
# Setup the include and linker paths
# ##############################################################################
include_directories(${CMAKE_SOURCE_DIR} ${CMAKE_BINARY_DIR})
# ##############################################################################
# Generate proto headers
# ##############################################################################
include_directories(${PROTOBUF_INCLUDE_DIR})

set(Protobuf_IMPORT_DIRS ${Protobuf_IMPORT_DIRS} ${PROJECT_SOURCE_DIR})
set(PROTOBUF_GENERATE_CPP_APPEND_PATH Off)
set(protos_src
    ${PROJECT_SOURCE_DIR}/satnogs-comms-proto/satnogs-comms.proto
    ${PROJECT_SOURCE_DIR}/satnogs-comms-proto/telecommand.proto
    ${PROJECT_SOURCE_DIR}/satnogs-comms-proto/telemetry.proto
    ${PROJECT_SOURCE_DIR}/satnogs-comms-proto/ota.proto)

# ##############################################################################
# Installation details
# ##############################################################################
include(CPack)

add_executable(
  satnogs-comms-ctrl
  ${protos_src} src/main.cpp src/transport.cpp src/can_transport.cpp
  src/gr_zmq_transport.cpp src/logger.cpp)

target_include_directories(satnogs-comms-ctrl PRIVATE "${PROJECT_BINARY_DIR}")
target_include_directories(satnogs-comms-ctrl
                           PUBLIC ${PROJECT_SOURCE_DIR}/cli/include () ftxui)

target_link_libraries(
  satnogs-comms-ctrl
  PUBLIC m
         ${CMAKE_THREAD_LIBS_INIT}
         ${Boost_SYSTEM_LIBRARY}
         protobuf::libprotobuf
         yaml-cpp::yaml-cpp
         spdlog::spdlog
         cppzmq
         gnuradio::gnuradio-pmt
         nlohmann_json::nlohmann_json)
protobuf_generate(TARGET satnogs-comms-ctrl)
